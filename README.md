## C++ CMake 

By default, the project will be compiled in ./build and will be installed in ./dist

# Adittional settings
In the building part, either in console mode either in gui mode, you can add/change this value 
to increase cmake performance

    cmake .. \
      -DCMAKE_INSTALL_PREFIX={$CMAKE_SOURCE_DIR}/dist \
      -DCMAKE_PREFIX_PATH={$third_party_libraries} \
      -DBUILD_SHARED_LIBS=ON \
      -DCMAKE_BUILD_TYPE=Release \
      -DDLIB_USE_BLAS=ON \
      -DDLIB_USE_CUDA=ON \
      -DUSE_AVX_INSTRUCTIONS=ON \

## Building Template

cmake should be called from a separate folder, setting the `CMAKE_BUILD_TYPE` to either `Debug` or `Release` (this isn't needed for the Visual Studio Generator) and `CMAKE_PREFIX_PATH`
to the location of the third-party libraries. Use `-G` to change generators (if you want to use ninja on linux or similar)

If all dependencies are installed correctly, no errors will be given. Compiling can be done with the chosen build tool (make, ninja, Visual Studio etc), or cmake can call the chosen tool for you with: `cmake --build . --config Release`

To build the project:
* CMD
    $ mkdir build
    $ cd build
    - Linux
        $ cmake .. -DCMAKE_PREFIX_PATH={$third_party_libraries}
        + Compilation
        The code can be compiled with the provided makefile using the standard `make command
        $ make -j4
    - Windows
        $ cmake .. -DCMAKE_PREFIX_PATH={$third_party_libraries}
        + Compilation
        $ cmake --build . --config Release --target INSTALL

* CMAKE GUI
    - Download CMake v3.13 from `https://github.com/Kitware/CMake/releases/tag/v3.13.5` (there are some problems with the most recent version), unzip them and open `bin\cmake-gui.exe` or `bin\cmake-gui`
    - Select the location of the source code (repository location) and the binaries (usually `repository\build`)
    - create new variable CMAKE_PREFIX_PATH and in the value write {$third_party_libraries}
    - press configure buttom
    - if the process end without any error, then press Generate buttom
    - Build INSTALL within Visual Studio

## Running Template

Add the root path of the library, using relatives, to facilitate cmake find_package instruction:
    - set(LIB_ROOT "${CMAKE_PREFIX_PATH};${CMAKE_PREFIX_PATH}/LIB")
    - find_package(LIB REQUIRED PATHS ${LIB_ROOT})

# Linux

In ./cmake/install_3party_libraries.cmake create variables with the template $set(linux_{lib_name}}_source "{lib_name}.so") and specify in the target the new name if it is necessary, then libraries will be installed at the lib folder, during the installing part

# Windows

In ./cmake/install_3party_libraries.cmake create variables with the template $set(windows_{lib_name}}_source "{lib_name}.dll") to install a library in lib (.lib) and dist (.dll) folder, during the installing part. Some libraries will be directly embedded in the code, therefore, they will not have a lib file

## Requirements

* CMAKE >= 3.13.5
* Visual Studio 17