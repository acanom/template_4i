#!/usr/bin/env python
########################################
# \file   build.py
# \date   01/01/2014
# \brief  Automated build script for C++ projects using CMake.
# \GitHub https://github.com/calebwherry/example-cmake-project
########################################

import sys
from sys import argv
import os
from os import path, mkdir, chdir, remove

import shutil
from shutil import rmtree
import multiprocessing
import subprocess
import time
import argparse
from platform import system
from datetime import datetime
from glob import glob
from colorama import init, Fore

global path_dist, thirdparty_path

# Global start time:
start_time = time.time()
def copy_dll(path_source, path_dst):
    if hasattr(sys, 'getwindowsversion'):
        for root, dirs, files in os.walk(path_source):
            for file in files:
                if file.endswith(".dll"):
                    #print(os.path.join(root, file))
                    try:
                        src_file = os.path.join(root, file)
                        dst_file = os.path.join(path_dst, file)
                        if not os.path.exists(dst_file):
                            shutil.copy(src_file, dst_file)
                            print(f"File {file} copied successfully.")
                        else:
                            print(f"File {file} already exists.")

                    # If source and destination are same
                    except shutil.SameFileError:
                        print(
                            f"File {file}: Source and destination represents the same file.")

                    # If there is any permission issue
                    except PermissionError:
                        print(f"File {file}: Permission denied.")

                    # For other errors
                    except BaseException:
                        print(
                            f"File {file}: Error occurred while copying file.")

def elapsedTime(time1, time2):

    elapsedTime = time2 - time1
    totalHours = int(elapsedTime / 3600)
    totalMinutes = int(elapsedTime / 60) % 60
    totalSeconds = elapsedTime % 60

    if totalHours != 0:
        elapsedTimeString = "{:d} hours, {:d} minutes, & {:.4f} seconds.".format(
            totalHours, totalMinutes, totalSeconds)
    elif totalMinutes != 0:
        elapsedTimeString = "{:d} minutes & {:.4f} seconds.".format(
            totalMinutes, totalSeconds)
    else:
        elapsedTimeString = "{:.4f} seconds.".format(totalSeconds)

    return elapsedTimeString

def displayLog(log):

    print('')
    print(Fore.MAGENTA + '#############################')
    print(Fore.MAGENTA + 'Dumping log file: ' + log.name)
    print(Fore.MAGENTA + '#############################')
    print('')

    # Open file and print it to stdout:
    with open(log.name, 'r') as fin:
        print(fin.read())

    print('')
    print(Fore.MAGENTA + '#############################')
    print(Fore.MAGENTA + 'End log dump.')
    print(Fore.MAGENTA + '#############################')
    print('')

def sysCall(cmds, log, pad="", shellExec=False):
    if len(cmds) == 1:
        cmds = cmds[0]
    else:
        cmds = " ".join(cmds)
    # Print command and run:
    print(Fore.CYAN + pad + "Running '" + cmds + "'...", end=' ')
    time.sleep(2)
    start_time = time.time()
    error_code = subprocess.call(
        cmds,
        shell=shellExec,
        stdout=log,
        stderr=subprocess.STDOUT)
    end_time = time.time()

    if error_code != 0:
        print(Fore.RED + 'ERROR!!! Please see log output below!')
        displayLog(log)
        # completeScript(1)
    print(Fore.GREEN + 'done. ' + elapsedTime(start_time, end_time))
    return error_code

def completeScript(exitCode=0):

    # Elapsed time:
    stopTime = time.time()

    print('Execution time: ' + elapsedTime(start_time, stopTime))
    print('')

    # Script completion output:
    if exitCode == 0:
        print(
            Fore.GREEN +
            'Script completed successfully: {:d}.'.format(exitCode))
    else:
        print(
            Fore.RED +
            'Script completed UNsuccessfully: {:d}.'.format(exitCode))
    print('')

    # Exit:
    sys.exit(exitCode)

# UNIX build:
def unixBuild(log, args):

    print('')
    print(Fore.CYAN + 'UNIX install starting...')

    # Execute build commands:
#  sysCall(["make", "-j"+str(args.num_cpus)], log, "\t")
    time.sleep(1)
    # sysCall(["make", "-j"ma+str(args.num_cpus), "test"], log, "\t")
    sysCall(["make", "-j" + str(args.num_cpus), "install"], log, "\t")
    time.sleep(1)
    print(Fore.GREEN + 'UNIX install complete!')
    print('')
    time.sleep(1)

# Windows build:
def windowsBuild(log, args, clone=False):
    global path_dist, thirdparty_path

    print('')
    print(Fore.CYAN + 'Windows install starting...')

    # Execute build commands:
#  sysCall(["msbuild", "ALL_BUILD.vcxproj"], log, "\t")
#  time.sleep(1)
#  #sysCall(["msbuild", "RUN_TESTS.vcxproj"], log, "\t")
#  sysCall(["msbuild", "ZERO_CHECK.vcxproj"], log, "\t")
#  time.sleep(1)
#  sysCall(["msbuild", "INSTALL.vcxproj"], log, "\t")
#  time.sleep(1)

    cmd_install = ['cmake --build . --config Release --target INSTALL']
    sysCall(cmd_install, log, "\t", shellExec=True)

    if clone:
        print("Cloning dll libraries")
        copy_dll(thirdparty_path, path_dist + '/bin')

    print(Fore.GREEN + 'Windows install complete!')
    print('')
    time.sleep(1)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-b",
        "--build-type",
        help="Build type (default: Release).",
        choices=[
            "Debug",
            "Release",
            "RelWithDebInfo",
            "MinSizeRel"],
        default="Release")
    parser.add_argument(
        "-c",
        "--clean",
        help="Remove build directory in current working directory matching 'build_${BUILD_TYPE}' then continue build",
        action="store_true")
    parser.add_argument(
        "-g",
        "--build-generator",
        help="Build generator type that CMake produces, see 'cmake --help' for the available options on this platform (default: CMake decides based on system settings).",
        type=str,
        default="")
    parser.add_argument(
        "-i",
        "--install-prefix",
        help="Prefix for the install directory.",
        type=str,
        default="")
    parser.add_argument(
        "-j",
        "--num-cpus",
        help="Number of CPUs for parallel builds (default: number of CPUs on machine)",
        type=int,
        default=multiprocessing.cpu_count())
    parser.add_argument(
        "-l",
        "--log-display",
        help="Display build log to stdout.",
        action="store_true")
    parser.add_argument(
        "-n",
        "--build-dir-name",
        help="Name of the build directory created (default: 'build_${BUILD_TYPE}')",
        type=str,
        default="")
    parser.add_argument(
        "-r",
        "--remove-build",
        help="Remove current build directory after completion.",
        action="store_true")
    parser.add_argument(
        "-s",
        "--static-build",
        help="Build and link libraries static instead of shared.",
        action="store_true")
    parser.add_argument(
        "-p",
        "--thirdparty-path",
        help="help",
        type=str,
        default="")
    parser.add_argument(
        "-t",
        "--thirdparty-clone",
        help="help",
        type=bool,
        default=False)

    args = parser.parse_args()
    thirdparty_path = args.thirdparty_path

    # Init colorama:
    init(autoreset=True)

    print('')
    print(Fore.GREEN + '######################')
    print(Fore.GREEN + 'Automated Build Script')
    print(Fore.GREEN + '######################')
    print('')

    timestamp = datetime.fromtimestamp(
        time.time()).strftime('%Y-%m-%d_%H-%M-%S')
    path_root = path.abspath(path.join(os.getcwd(), os.pardir))
    localOS = system()

    # Create build directories:
    build_path_name = ""
    if args.build_dir_name == "":
        build_path_name = "build"
    else:
        build_path_name = args.build_dir_name
    path_build = path.join(path_root, build_path_name)

    # Remove build directory if exists and clean specified:
    if args.clean:
        try:
            print(Fore.RED +
                "Removing build directory '" +
                path_build +
                "'... ",
                end='')
            rmtree(path_build)
            print(Fore.GREEN + 'done.')
            print('')
        except Exception as e:
            print(e)
#      print("No build directory to clean.")
            print('')

    # Create install directory if prefix was not supplied:
    if args.install_prefix == "":
        path_dist = path.join(path_root, 'dist')
    else:
        path_dist = path.join(path_root, args.install_prefix)
    if args.clean:
        try:
            print("Removing dist directory '" + path_dist + "'... ", end='')
            rmtree(path_dist)
            print(Fore.GREEN + 'done.')
            print('')
        except Exception as e:
            print(e)
            print('')
    try:
        mkdir(path_build)
    except BaseException:
        print(
            "Build directory '" +
            path_build +
            "' already exists; re-running build process.")
        print('')

    # Diplay build directory to screen:
    print('Build location: ' + Fore.MAGENTA + path_build + "\n")

    if args.static_build:
        sharedLibs = "OFF"
    else:
        sharedLibs = "ON"

    # Move into build directory:
    chdir(path_build)

    # Create logfile:
    logFile = path.join(path_build, 'build.log')
    log = open(logFile, 'w')

    # Check number of CPUs:
    if args.num_cpus <= 2:
        args.num_cpus = 1
    elif (args.num_cpus > 2) and (args.num_cpus <= 4):
        args.num_cpus = args.num_cpus - 1
    elif (args.num_cpus > 5) and (args.num_cpus <= 10):
        args.num_cpus = args.num_cpus - 2
    else:
        args.num_cpus = args.num_cpus - 4

    # Build up CMake command based on CLI options:
    cmakeCmd = [
        "cmake", "..",
        "-DCMAKE_INSTALL_PREFIX:PATH='" + path_dist + "'",
        "-DCMAKE_BUILD_TYPE:STRING=" + args.build_type,
        #    "-DBUILD_SHARED_LIBS:BOOL=" + sharedLibs,
        #    "-DCMAKE_INSTALL_SO_NO_EXE:BOOL=OFF"
    ]

    if args.thirdparty_path != "":
        cmakeCmd.append("-DCMAKE_PREFIX_PATH:PATH='" + thirdparty_path + "'")
    else:
        thirdparty_path = input("Introduce third party libraries path:")
    if args.build_generator != "":
        cmakeCmd.append('-G"' + args.build_generator + '"')

    error_code = sysCall(cmakeCmd, log)
    if error_code == 0:
        pass
    else:
        print(Fore.RED + "cmake cmd exit with an error, try cmake GUI")
        time.sleep(1)
        # delete all files in build
#        for f in os.listdir(path_build):
#            try:
#                os.remove(os.path.join(path_build, f))
#            except PermissionError:
#                pass
#        sysCall(["cmake-gui"], log)
#        # check build compilation done
#        if len(os.listdir(path_build)) <= 5:
#            sys.exit()
    try:
        mkdir(path_dist)
    except BaseException:
        reBuild = True
        print(
            "Dist directory '" +
            path_dist +
            "' already exists; re-running dist process.")
        print('')
    # Execute build based on platform from this point on:
    if localOS == 'Linux' or localOS == 'Darwin':
        unixBuild(log, args)
    elif localOS == 'Windows':
        windowsBuild(log, args, args.thirdparty_clone)
    else:
        print(Fore.RED + '**ERROR**: OS platform "' +
              localOS + '" not recognized; aborting!')
        completeScript(1)

    # Display log if cmd argument set:
    if args.log_display:
        displayLog(log)

    # Close log file:
    log.close()

    # Remove build directory if flag set
    chdir(path_root)
    if args.remove_build:
        print('Removing current build directory...', end=' ')
        rmtree(path_build)
        print(Fore.GREEN + 'done.')
        print('')

    # Exit cleanly:
    time.sleep(5)
    #input("Press any key to continue:")
    if localOS == 'Linux' or localOS == 'Darwin':
        os.system('read -s -n 1 -p "Press any key to continue..."')
    elif localOS == 'Windows':
        os.system("pause")
        
    completeScript()