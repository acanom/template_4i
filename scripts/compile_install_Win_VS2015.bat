@echo off

REM Build project:
echo "-> Building project..."
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64
python autoBuild.py --clean --build-type="Release" --build-generator="Visual Studio 14 2015 Win64" --clean --build-generator="Visual Studio 15 2017 Win64" --thirdparty-path "path_adrean_libraries\windows_x86_64" -t False
