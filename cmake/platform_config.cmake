# always generate position independent code
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
# minimum c++ standard required
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED on)
enable_language(CXX)

# Compiler CLANG
if(CMAKE_COMPILER_IS_GNUCC OR "${CMAKE_C_COMPILER_ID}" MATCHES "Clang" OR "${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
  set(CMAKE_COMPILER_IS_GNUCC_OR_CLANG TRUE)
  if("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang" OR "${CMAKE_CXX_COMPILER_ID}" MATCHES "clang")
    set(CMAKE_COMPILER_IS_CLANG TRUE)
  else()
    set(CMAKE_COMPILER_IS_CLANG FALSE)
  endif()
else()
  set(CMAKE_COMPILER_IS_GNUCC_OR_CLANG FALSE)
  set(CMAKE_COMPILER_IS_CLANG FALSE)
endif()

# we use this variable for each executable, but it's only set to a non-empty string on Windows
set(CMAKE_DEBUG_POSTFIX "")

if("${CMAKE_SYSTEM_NAME}" MATCHES "Linux")

elseif("${CMAKE_SYSTEM_NAME}" MATCHES "Windows")
  if(USE_AVX2)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  /arch:AVX2")
  endif()
elseif("${CMAKE_SYSTEM_NAME}" MATCHES "Darwin")

endif()

if(CMAKE_HOST_SYSTEM_PROCESSOR MATCHES "x86")
  set(IS_X86 TRUE)
else()
  set(IS_X86 FALSE)
endif()