if(NOT WIN32)
  string(ASCII 27 Esc)
  set(ColourReset "${Esc}[m")
  set(ColourBold  "${Esc}[1m")
  set(Red         "${Esc}[31m")
  set(Green       "${Esc}[32m")
  set(Yellow      "${Esc}[33m")
  set(Blue        "${Esc}[34m")
  set(Magenta     "${Esc}[35m")
  set(Cyan        "${Esc}[36m")
  set(White       "${Esc}[37m")
  set(BoldRed     "${Esc}[1;31m")
  set(BoldGreen   "${Esc}[1;32m")
  set(BoldYellow  "${Esc}[1;33m")
  set(BoldBlue    "${Esc}[1;34m")
  set(BoldMagenta "${Esc}[1;35m")
  set(BoldCyan    "${Esc}[1;36m")
  set(BoldWhite   "${Esc}[1;37m")
endif()

function(logger)
  list(GET ARGV 0 loggerType)
  list(GET ARGN 0 optArg1)
  # General info
  if(loggerType STREQUAL FATAL_ERROR OR loggerType STREQUAL SEND_ERROR)
    list(REMOVE_AT ARGV 0)
    message(${loggerType} "${BoldRed}${ARGV}${ColourReset}")
  elseif(loggerType STREQUAL WARN)
    list(REMOVE_AT ARGV 0)
    message(${loggerType} " ${BoldYellow}${ARGV}${ColourReset}")
  elseif(loggerType STREQUAL AUTHOR_WARNING)
    list(REMOVE_AT ARGV 0)
    message(${loggerType} " ${BoldCyan}${ARGV}${ColourReset}")
  elseif(loggerType STREQUAL STATUS)
    list(REMOVE_AT ARGV 0)
    message(${loggerType} "${Green}${ARGV}${ColourReset}")
  elseif(loggerType STREQUAL GENERAL)
    list(REMOVE_AT ARGV 0)
    message(STATUS "${Green}─────────────────────────────────────────────────────────────────${ColourReset}")
    message(STATUS "${Green}PROJECT PATH: ${CMAKE_SOURCE_DIR}")
    message(STATUS "${Green}BUILDING PATH: ${CMAKE_BINARY_DIR}")
    message(STATUS "${Green}INSTALLATION PATH: ${CMAKE_INSTALL_PREFIX}")
    message(STATUS "${Green}3RD PARTY LIBRARIES PATH: ${CMAKE_PREFIX_PATH}")
    message(STATUS "${Green}─────────────────────────────────────────────────────────────────${ColourReset}")
  else()
    message("${ARGV}")
  endif()
endfunction()

function(initial_msg)
  string(TIMESTAMP TODAY "%Y/%m/%d %H:%M:%S")
  message("Building start at ${TODAY}")
  message(STATUS "
██████╗░██╗░░░██╗██╗██╗░░░░░██████╗░██╗███╗░░██╗░██████╗░
██╔══██╗██║░░░██║██║██║░░░░░██╔══██╗██║████╗░██║██╔════╝░
██████╦╝██║░░░██║██║██║░░░░░██║░░██║██║██╔██╗██║██║░░██╗░
██╔══██╗██║░░░██║██║██║░░░░░██║░░██║██║██║╚████║██║░░╚██╗
██████╦╝╚██████╔╝██║███████╗██████╔╝██║██║░╚███║╚██████╔╝
╚═════╝░░╚═════╝░╚═╝╚══════╝╚═════╝░╚═╝╚═╝░░╚══╝░╚═════╝░

██████╗░██████╗░░█████╗░░░░░░██╗███████╗░█████╗░████████╗░░░░░░░░░
██╔══██╗██╔══██╗██╔══██╗░░░░░██║██╔════╝██╔══██╗╚══██╔══╝░░░░░░░░░
██████╔╝██████╔╝██║░░██║░░░░░██║█████╗░░██║░░╚═╝░░░██║░░░░░░░░░░░░
██╔═══╝░██╔══██╗██║░░██║██╗░░██║██╔══╝░░██║░░██╗░░░██║░░░░░░░░░░░░
██║░░░░░██║░░██║╚█████╔╝╚█████╔╝███████╗╚█████╔╝░░░██║░░░██╗██╗██╗
╚═╝░░░░░╚═╝░░╚═╝░╚════╝░░╚════╝░╚══════╝░╚════╝░░░░╚═╝░░░╚═╝╚═╝╚═╝
    "
  )
endfunction()

function(ending_msg)
  string(TIMESTAMP TODAY_AFTER "%Y/%m/%d %H:%M:%S")
  message("The Build process finish successfully at ${TODAY_AFTER}")
endfunction()

function(check_found lib_status lib_name)
  if (${lib_status})
    message(STATUS "${lib_name} package found")
  else()
    message(FATAL_ERROR "${lib_name} package not found \n")
  endif()
endfunction()