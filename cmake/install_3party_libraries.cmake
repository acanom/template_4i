# Linux variables defining files to copy and how to name them
set(linux_lib_lib_source "liblib.so.5")
set(linux_lib_lib_target "liblib.so")

# Windows variables defining files to copy and how to name them
set(windows_lib_lib_source "lib.dll")

# OSX variables defining files to copy and how to name them


function(install_third_party target target_name)
  message(STATUS "Copying (installing) ${target_name} into ${CMAKE_SOURCE_DIR}/dist")

  if("${CMAKE_SYSTEM_NAME}" MATCHES "Linux")
    if(linux_${target_name}_source AND linux_${target_name}_target)
      install(FILES $<TARGET_FILE_DIR:${target}>/${linux_${target_name}_source} DESTINATION lib RENAME "${linux_${target_name}_target}")
    else()
      message(FATAL_ERROR " Trying to install unrecognised third-party library ${target_name}")
    endif()
    
  elseif("${CMAKE_SYSTEM_NAME}" MATCHES "Windows")
    if(windows_${target_name}_source)
      install(FILES $<TARGET_FILE_DIR:${target}>/${windows_${target_name}_source} DESTINATION bin)
    else()
      message(WARN " Trying to install unrecognised third-party library ${target_name}")
    endif()

  elseif("${CMAKE_SYSTEM_NAME}" MATCHES "Darwin")
  endif()

endfunction()

MACRO(COPY_FILE lib_file mode)
  if("${mode}" MATCHES "BUILD")
    message(STATUS "${mode}: Manual installation (copy) ${lib_file}")
    file(COPY ${CMAKE_PREFIX_PATH}/${lib_file} DESTINATION bin)
  elseif("${mode}" MATCHES "INSTALL")
    message(STATUS "${mode}: Manual installation (copy) ${CMAKE_PREFIX_PATH}${lib_file}")
    install(FILES ${CMAKE_PREFIX_PATH}/${lib_file} DESTINATION bin)
  #elseif("${mode}" MATCHES "INSTALL")
    #message(STATUS "${mode}: Manual installation (copy) ${CMAKE_PREFIX_PATH}${lib_file}")
    #ADD_CUSTOM_COMMAND(TARGET ${ARGV}
        #POST_BUILD COMMAND ${CMAKE_COMMAND}
        #ARGS -E copy "${CMAKE_PREFIX_PATH}${lib_file}" ${CMAKE_INSTALL_PREFIX}/bin
    #)
  endif()
ENDMACRO()

