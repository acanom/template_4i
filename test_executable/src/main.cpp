#include "library_deeper.h"
#include "library1.h"
#include "library.h"

#include <iostream>
#include <conio.h>

int main() {
    std::cout << "Hello World! \n";
    printHello();
    printHelloDeeper();
    printHello_1();
    getch();
    return 0;
}